Rails.application.routes.draw do
  get 'tweets/index', to: 'tweets#index'
  root 'tweets#index'
  resources :tweets
end
